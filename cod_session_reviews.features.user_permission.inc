<?php
/**
 * @file
 * cod_session_reviews.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cod_session_reviews_user_default_permissions() {
  $permissions = array();

  // Exported permission: create review content.
  $permissions['create review content'] = array(
    'name' => 'create review content',
    'roles' => array(
      0 => 'administrator',
      1 => 'reviewer',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any review content.
  $permissions['delete any review content'] = array(
    'name' => 'delete any review content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own review content.
  $permissions['delete own review content'] = array(
    'name' => 'delete own review content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any review content.
  $permissions['edit any review content'] = array(
    'name' => 'edit any review content',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own review content.
  $permissions['edit own review content'] = array(
    'name' => 'edit own review content',
    'roles' => array(
      0 => 'administrator',
      1 => 'reviewer',
    ),
    'module' => 'node',
  );

  return $permissions;
}
