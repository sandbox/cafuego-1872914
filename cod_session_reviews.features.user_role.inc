<?php
/**
 * @file
 * cod_session_reviews.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function cod_session_reviews_user_default_roles() {
  $roles = array();

  // Exported role: reviewer.
  $roles['reviewer'] = array(
    'name' => 'reviewer',
    'weight' => '5',
  );

  return $roles;
}
