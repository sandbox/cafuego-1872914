<?php
/**
 * @file
 * cod_session_reviews.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function cod_session_reviews_field_default_fields() {
  $fields = array();

  // Exported field: 'node-review-body'.
  $fields['node-review-body'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(
        0 => 'node',
      ),
      'field_name' => 'body',
      'field_permissions' => array(
        'type' => '0',
      ),
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(
        'field_permissions' => array(
          'create' => 'create',
          'edit' => 0,
          'edit own' => 'edit own',
          'view' => 'view',
          'view own' => 0,
        ),
      ),
      'translatable' => '1',
      'type' => 'text_with_summary',
    ),
    'field_instance' => array(
      'bundle' => 'review',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Please provide private feedback about the session. This feedback is visible to you, other reviewers and to the event organizers. The session author cannot see this feedback.',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '1',
        ),
        'listing' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'review' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(
            'trim_length' => 600,
          ),
          'type' => 'text_summary_or_trimmed',
          'weight' => 1,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'body',
      'label' => 'Private Feedback',
      'required' => 1,
      'settings' => array(
        'display_summary' => 1,
        'text_processing' => '1',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '20',
          'summary_rows' => 5,
        ),
        'type' => 'text_textarea_with_summary',
        'weight' => '3',
      ),
    ),
  );

  // Exported field: 'node-review-field_review_attachment'.
  $fields['node-review-field_review_attachment'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_review_attachment',
      'field_permissions' => array(
        'type' => '1',
      ),
      'foreign keys' => array(
        'fid' => array(
          'columns' => array(
            'fid' => 'fid',
          ),
          'table' => 'file_managed',
        ),
      ),
      'indexes' => array(
        'fid' => array(
          0 => 'fid',
        ),
      ),
      'locked' => '0',
      'module' => 'file',
      'settings' => array(
        'display_default' => 0,
        'display_field' => 0,
        'uri_scheme' => 'private',
      ),
      'translatable' => '0',
      'type' => 'file',
    ),
    'field_instance' => array(
      'bundle' => 'review',
      'deleted' => '0',
      'description' => 'If you have notes in a file, please attach them here. The session author and event conveners will be able to download this document. Please ensure you leave no identifying information in your file, including Word metadata.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'file',
          'settings' => array(),
          'type' => 'file_default',
          'weight' => '2',
        ),
        'listing' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'review' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_review_attachment',
      'label' => 'Attachment',
      'required' => 0,
      'settings' => array(
        'description_field' => 0,
        'file_directory' => 'review',
        'file_extensions' => 'txt pdf odt odp doc docx ppt pptx',
        'max_filesize' => '',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'file',
        'settings' => array(
          'insert' => 0,
          'insert_absolute' => FALSE,
          'insert_class' => '',
          'insert_default' => array(
            0 => 'auto',
          ),
          'insert_styles' => array(
            0 => 'auto',
          ),
          'insert_width' => '',
          'progress_indicator' => 'throbber',
        ),
        'type' => 'file_generic',
        'weight' => '8',
      ),
    ),
  );

  // Exported field: 'node-review-field_review_feedback'.
  $fields['node-review-field_review_feedback'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_review_feedback',
      'field_permissions' => array(
        'type' => '0',
      ),
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(),
      'translatable' => '0',
      'type' => 'text_with_summary',
    ),
    'field_instance' => array(
      'bundle' => 'review',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Please provide the author feedback about their session. This feedback is visible to you, to the session author, to other reviewers and to the event organizers.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => 4,
        ),
        'listing' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'review' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_review_feedback',
      'label' => 'Author Feedback',
      'required' => 0,
      'settings' => array(
        'better_formats' => array(
          'allowed_formats' => array(
            'filtered_html' => 'filtered_html',
            'full_html' => 'full_html',
            'plain_text' => 'plain_text',
            'raw_html' => 'raw_html',
          ),
          'allowed_formats_toggle' => 0,
          'default_order_toggle' => 0,
          'default_order_wrapper' => array(
            'formats' => array(
              'filtered_html' => array(
                'weight' => '0',
              ),
              'full_html' => array(
                'weight' => '0',
              ),
              'plain_text' => array(
                'weight' => '10',
              ),
              'raw_html' => array(
                'weight' => '0',
              ),
            ),
          ),
        ),
        'display_summary' => 0,
        'text_processing' => '1',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'maxlength_js' => '',
          'maxlength_js_enforce' => 0,
          'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
          'maxlength_js_truncate_html' => 0,
          'rows' => '20',
          'summary_rows' => 5,
        ),
        'type' => 'text_textarea_with_summary',
        'weight' => '6',
      ),
    ),
  );

  // Exported field: 'node-review-field_review_originality'.
  $fields['node-review-field_review_originality'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_review_originality',
      'field_permissions' => array(
        'type' => '1',
      ),
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'fivestar',
      'settings' => array(
        'axis' => 'session_originality',
      ),
      'translatable' => '0',
      'type' => 'fivestar',
    ),
    'field_instance' => array(
      'bundle' => 'review',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'How original is this session proposal? Has this speaker presented this session before? Does it contain new content or is it identical to sessions given at other conferences?
Not original = 1 star, completely original = 5 stars.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'fivestar',
          'settings' => array(
            'expose' => TRUE,
            'style' => 'average',
            'text' => 'average',
            'widget' => array(
              'fivestar_widget' => NULL,
            ),
          ),
          'type' => 'fivestar_formatter_default',
          'weight' => 5,
        ),
        'listing' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'review' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_review_originality',
      'label' => 'Originality',
      'required' => 1,
      'settings' => array(
        'allow_clear' => 1,
        'stars' => '5',
        'target' => 'field_review_session',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'fivestar',
        'settings' => array(
          'widget' => array(
            'fivestar_widget' => 'profiles/cod/modules/contrib/fivestar/widgets/outline/outline.css',
          ),
        ),
        'type' => 'stars',
        'weight' => '4',
      ),
    ),
  );

  // Exported field: 'node-review-field_review_quality'.
  $fields['node-review-field_review_quality'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_review_quality',
      'field_permissions' => array(
        'type' => '1',
      ),
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'fivestar',
      'settings' => array(
        'axis' => 'session_quality',
      ),
      'translatable' => '0',
      'type' => 'fivestar',
    ),
    'field_instance' => array(
      'bundle' => 'review',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'How well-written is this session proposal? Does the abstract make sense or is it a set of unrelated ramblings? Is it full of typos and spelling errors?
Terrible = 1 star, excellent = 5 stars.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'fivestar',
          'settings' => array(
            'expose' => TRUE,
            'style' => 'average',
            'text' => 'average',
            'widget' => array(
              'fivestar_widget' => NULL,
            ),
          ),
          'type' => 'fivestar_formatter_default',
          'weight' => 6,
        ),
        'listing' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'review' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_review_quality',
      'label' => 'Quality',
      'required' => 1,
      'settings' => array(
        'allow_clear' => 1,
        'stars' => '5',
        'target' => 'field_review_session',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'fivestar',
        'settings' => array(
          'widget' => array(
            'fivestar_widget' => 'profiles/cod/modules/contrib/fivestar/widgets/outline/outline.css',
          ),
        ),
        'type' => 'stars',
        'weight' => '5',
      ),
    ),
  );

  // Exported field: 'node-review-field_review_recommendation'.
  $fields['node-review-field_review_recommendation'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_review_recommendation',
      'field_permissions' => array(
        'type' => '1',
      ),
      'foreign keys' => array(),
      'indexes' => array(
        'value' => array(
          0 => 'value',
        ),
      ),
      'locked' => '0',
      'module' => 'list',
      'settings' => array(
        'allowed_values' => array(
          1 => 'I want this session to be <em>accepted</em>',
          -1 => 'I want this session to be <em>rejected</em>',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '0',
      'type' => 'list_integer',
    ),
    'field_instance' => array(
      'bundle' => 'review',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Your over-all opinion on this session',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'list',
          'settings' => array(),
          'type' => 'list_default',
          'weight' => 8,
        ),
        'listing' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'review' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_review_recommendation',
      'label' => 'Recommendation',
      'required' => 1,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '7',
      ),
    ),
  );

  // Exported field: 'node-review-field_review_session'.
  $fields['node-review-field_review_session'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_review_session',
      'field_permissions' => array(
        'type' => '1',
      ),
      'foreign keys' => array(
        'node' => array(
          'columns' => array(
            'target_id' => 'nid',
          ),
          'table' => 'node',
        ),
      ),
      'indexes' => array(
        'target_id' => array(
          0 => 'target_id',
        ),
      ),
      'locked' => '0',
      'module' => 'entityreference',
      'settings' => array(
        'handler' => 'base',
        'handler_settings' => array(
          'behaviors' => array(
            'views-select-list' => array(
              'status' => 0,
            ),
          ),
          'sort' => array(
            'type' => 'none',
          ),
          'target_bundles' => array(
            'session' => 'session',
          ),
        ),
        'target_type' => 'node',
      ),
      'translatable' => '0',
      'type' => 'entityreference',
    ),
    'field_instance' => array(
      'bundle' => 'review',
      'default_value' => NULL,
      'default_value_function' => 'entityreference_prepopulate_field_default_value',
      'deleted' => '0',
      'description' => 'The session that this review applies to.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '3',
        ),
        'listing' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'review' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_review_session',
      'label' => 'Session',
      'required' => 1,
      'settings' => array(
        'behaviors' => array(
          'prepopulate' => array(
            'action' => 'hide',
            'fallback' => 'redirect',
            'skip_perm' => '0',
            'status' => 1,
          ),
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => '1',
      ),
    ),
  );

  // Exported field: 'node-review-field_review_significance'.
  $fields['node-review-field_review_significance'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_review_significance',
      'field_permissions' => array(
        'type' => '1',
      ),
      'foreign keys' => array(),
      'indexes' => array(),
      'locked' => '0',
      'module' => 'fivestar',
      'settings' => array(
        'axis' => 'session_significance',
      ),
      'translatable' => '0',
      'type' => 'fivestar',
    ),
    'field_instance' => array(
      'bundle' => 'review',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => 'Is the session topic current and relevant to delegates? Does the session author know the topic or project and are they involved with it?
Irrelevant = 1 star, must-have = 5 stars.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'fivestar',
          'settings' => array(
            'expose' => TRUE,
            'style' => 'average',
            'text' => 'average',
            'widget' => array(
              'fivestar_widget' => NULL,
            ),
          ),
          'type' => 'fivestar_formatter_default',
          'weight' => 7,
        ),
        'listing' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'review' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_review_significance',
      'label' => 'Significance',
      'required' => 1,
      'settings' => array(
        'allow_clear' => 1,
        'stars' => '5',
        'target' => 'field_review_session',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'fivestar',
        'settings' => array(
          'widget' => array(
            'fivestar_widget' => 'profiles/cod/modules/contrib/fivestar/widgets/outline/outline.css',
          ),
        ),
        'type' => 'stars',
        'weight' => '6',
      ),
    ),
  );

  // Exported field: 'node-session-field_reviewers'.
  $fields['node-session-field_reviewers'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_reviewers',
      'field_permissions' => array(
        'type' => '2',
      ),
      'foreign keys' => array(
        'users' => array(
          'columns' => array(
            'target_id' => 'uid',
          ),
          'table' => 'users',
        ),
      ),
      'indexes' => array(
        'target_id' => array(
          0 => 'target_id',
        ),
      ),
      'locked' => '0',
      'module' => 'entityreference',
      'settings' => array(
        'handler' => 'views',
        'handler_settings' => array(
          'behaviors' => array(
            'views-select-list' => array(
              'status' => 0,
            ),
          ),
          'view' => array(
            'args' => array(),
            'display_name' => 'entityreference_2',
            'view_name' => 'user_reference_select',
          ),
        ),
        'target_type' => 'user',
      ),
      'translatable' => '0',
      'type' => 'entityreference',
    ),
    'field_instance' => array(
      'bundle' => 'session',
      'default_value' => NULL,
      'default_value_function' => '',
      'deleted' => '0',
      'description' => 'Select all reviewers that should be able to review this session. This field is visible only to conference organisers and paper chairs.',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'entityreference',
          'settings' => array(
            'link' => FALSE,
          ),
          'type' => 'entityreference_label',
          'weight' => '13',
        ),
        'listing' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'review' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_reviewers',
      'label' => 'Reviewers',
      'required' => 0,
      'settings' => array(
        'behaviors' => array(
          'prepopulate' => array(
            'status' => 0,
          ),
        ),
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'entityreference',
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'maxlength_js_label' => 'Content limited to @limit characters, remaining: <strong>@remaining</strong>',
          'path' => '',
          'size' => '60',
        ),
        'type' => 'entityreference_autocomplete',
        'weight' => '1',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Attachment');
  t('Author Feedback');
  t('How original is this session proposal? Has this speaker presented this session before? Does it contain new content or is it identical to sessions given at other conferences?
Not original = 1 star, completely original = 5 stars.');
  t('How well-written is this session proposal? Does the abstract make sense or is it a set of unrelated ramblings? Is it full of typos and spelling errors?
Terrible = 1 star, excellent = 5 stars.');
  t('If you have notes in a file, please attach them here. The session author and event conveners will be able to download this document. Please ensure you leave no identifying information in your file, including Word metadata.');
  t('Is the session topic current and relevant to delegates? Does the session author know the topic or project and are they involved with it?
Irrelevant = 1 star, must-have = 5 stars.');
  t('Originality');
  t('Please provide private feedback about the session. This feedback is visible to you, other reviewers and to the event organizers. The session author cannot see this feedback.');
  t('Please provide the author feedback about their session. This feedback is visible to you, to the session author, to other reviewers and to the event organizers.');
  t('Private Feedback');
  t('Quality');
  t('Recommendation');
  t('Reviewers');
  t('Select all reviewers that should be able to review this session. This field is visible only to conference organisers and paper chairs.');
  t('Session');
  t('Significance');
  t('The session that this review applies to.');
  t('Your over-all opinion on this session');

  return $fields;
}
