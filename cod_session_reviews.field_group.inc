<?php
/**
 * @file
 * cod_session_reviews.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function cod_session_reviews_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_review_ratings|node|review|form';
  $field_group->group_name = 'group_review_ratings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'review';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Ratings',
    'weight' => '2',
    'children' => array(
      0 => 'field_review_originality',
      1 => 'field_review_quality',
      2 => 'field_review_recommendation',
      3 => 'field_review_significance',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_review_ratings|node|review|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_session_review|node|session|review';
  $field_group->group_name = 'group_session_review';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'session';
  $field_group->mode = 'review';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Session',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_experience',
      2 => 'field_speakers',
      3 => 'field_track',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
      ),
    ),
  );
  $export['group_session_review|node|session|review'] = $field_group;

  return $export;
}
