<?php
/**
 * @file
 * cod_session_reviews.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function cod_session_reviews_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management:admin/conference/reviews
  $menu_links['management:admin/conference/reviews'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/conference/reviews',
    'router_path' => 'admin/conference/reviews',
    'link_title' => 'Reviews',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
    'parent_path' => 'admin/conference',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Reviews');


  return $menu_links;
}
