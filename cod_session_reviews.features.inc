<?php
/**
 * @file
 * cod_session_reviews.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cod_session_reviews_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cod_session_reviews_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function cod_session_reviews_node_info() {
  $items = array(
    'review' => array(
      'name' => t('Review'),
      'base' => 'node_content',
      'description' => t('Used by reviewers to review proposed sessions. Allows for voting and feedback for the submitter and other reviewers.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
